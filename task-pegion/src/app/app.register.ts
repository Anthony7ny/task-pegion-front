import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgModule } from '@angular/core';


declare var jQuery;
declare var $;


@Component ({
    selector : 'register-form',
    templateUrl : './app.register.html',
    styleUrls: ['./app.register.css']
})

export class RegisterComponent {
    complexForm : FormGroup;
  
    constructor(task_pigeon: FormBuilder){
      this.complexForm = task_pigeon.group({
        'firstName' : [null, Validators.required],
        'lastName' : [null, Validators.required],
        'userName' : [null, Validators.required],
        'checkbox' : [false, Validators.required ]
      })
    
    }
    
  
    submitForm(value: any){
      $('#firstName').val("");
      $('#lastName').val("");
      $('#userName').val("");
      console.log(value);

     
    }


    

    // changeIcon(){
    //   $(".checkbox-inline").css({"display":"none"}),
    //   $(".checkbox-inline1").css({"display":"block"})
    // }
    // change(){
    //   $(".checkbox-inline").css({"display":"block"}),
    //   $(".checkbox-inline1").css({"display":"none"})
    // }

  }